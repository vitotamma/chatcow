package com.google.firebase.udacity.ChatCow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.google.firebase.udacity.friendlychat.SapiAyrshire;
import com.google.firebase.udacity.friendlychat.SapiBrownSwiss;
import com.google.firebase.udacity.friendlychat.SapiGuernsey;
import com.google.firebase.udacity.friendlychat.SapiMurrayGrey;
import com.google.firebase.udacity.friendlychat.SapiRedSindhi;

public class ListSapi extends AppCompatActivity {

    public  String tangkap1;
    public  String tangkap2;
    Spinner perah;
    Spinner daging;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sapi);
        perah = (Spinner) findViewById(R.id.perah);
        daging = (Spinner) findViewById(R.id.Pedaging);

        Button pindah1= (Button) findViewById(R.id.cari1);
        Button pindah2= (Button) findViewById(R.id.cari2);

        pindah1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tangkap1= perah.getSelectedItem().toString();
                if(tangkap1.equals("Sapi Holstein – Friesien")){
                    Intent i = new Intent(ListSapi.this,SapiHolstein.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Ayrshire")){
                    Intent i = new Intent(ListSapi.this,SapiAyrshire.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Brown Swiss")){
                    Intent i = new Intent(ListSapi.this,SapiBrownSwiss.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Guernsey")){
                    Intent i = new Intent(ListSapi.this,SapiGuernsey.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Jersey")){
                    Intent i = new Intent(ListSapi.this,SapiJersey.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Gir")){
                    Intent i = new Intent(ListSapi.this,SapiGir.class);
                    startActivity(i);
                }if(tangkap1.equals("Sapi Red Sindhi")){
                    Intent i = new Intent(ListSapi.this,SapiRedSindhi.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Sahiwal")){
                    Intent i = new Intent(ListSapi.this,SapiSahiwal.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Frieswal")){
                    Intent i = new Intent(ListSapi.this,SapiFrieswal.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Girolanda")){
                    Intent i = new Intent(ListSapi.this,SapiGirolanda.class);
                    startActivity(i);
                }
                if(tangkap1.equals("Sapi Jamaica Hope")){
                    Intent i = new Intent(ListSapi.this,SapiJamaicaHope.class);
                    startActivity(i);
                }
            }
        });
        pindah2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tangkap2= daging.getSelectedItem().toString();
                if(tangkap2.equals("Sapi Limousin")){
                    Intent i = new Intent(ListSapi.this,SapiLimousin.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Simmental")){
                    Intent i = new Intent(ListSapi.this,SapiSimmental.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Brahma")){
                    Intent i = new Intent(ListSapi.this,SapiBrahma.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Brangus")){
                    Intent i = new Intent(ListSapi.this,SapiBragus.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Aberdeen Angus")){
                    Intent i = new Intent(ListSapi.this,SapiAberdeenAngus.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Beefalo")){
                    Intent i = new Intent(ListSapi.this,SapiBeefalo.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Braford")){
                    Intent i = new Intent(ListSapi.this,SapiBraford.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Hereford")){
                    Intent i = new Intent(ListSapi.this,SapiHereford.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Dexter")){
                    Intent i = new Intent(ListSapi.this,SapiDexter.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Parthenais")){
                    Intent i = new Intent(ListSapi.this,SapiParthenais.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Belgian Blue")){
                    Intent i = new Intent(ListSapi.this,SapiBelgianBlue.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Droughtmaster")){
                    Intent i = new Intent(ListSapi.this,SapiDroughtmaster.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Murray Grey")){
                    Intent i = new Intent(ListSapi.this,SapiMurrayGrey.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Pinzgauer")){
                    Intent i = new Intent(ListSapi.this,SapiPinzgauer.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Ongole")){
                    Intent i = new Intent(ListSapi.this,SapiOngole.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Madura")){
                    Intent i = new Intent(ListSapi.this,SapiMadura.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Hereford")){
                    Intent i = new Intent(ListSapi.this,SapiHereford.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Bali")){
                    Intent i = new Intent(ListSapi.this,SapiBali.class);
                    startActivity(i);
                }
                if(tangkap2.equals("Sapi Aceh")){
                    Intent i = new Intent(ListSapi.this,SapiAceh.class);
                    startActivity(i);
                }
            }
        });
    }
    public void keChat(View view) {
        Intent intent = new Intent(ListSapi.this,MainActivity.class);
        startActivity(intent);
    }
}
